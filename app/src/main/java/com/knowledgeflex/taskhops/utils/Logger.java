package com.knowledgeflex.taskhops.utils;

import android.util.Log;

/**
 * Created by my on 6/4/2017.
 */

public class Logger {


    public Logger() {
    }

    public static void d(String tag , String message){
        Log.d(tag,message);
    }

    public static void e(String tag , String message){
        Log.e(tag,message);
    }
}
