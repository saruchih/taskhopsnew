package com.knowledgeflex.taskhops.utils;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;
import com.knowledgeflex.taskhops.R;
import com.knowledgeflex.taskhops.gps.GpsTrackingService;

/**
 * Created by saruchiarora on 28,November,2018
 */
public class CommonMethods {

    public static void showDialogOK(Context context, String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(context.getResources().getString(R.string.label_ok), okListener)
                .setNegativeButton(context.getResources().getString(R.string.label_cancel), okListener)
                .create()
                .show();
    }

    public static String[] getImeiNumber(Context context) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            showToast(context,context.getResources().getString(R.string.error_phone_state));
            return null;
        }else
            {
            String[] imeiArray= new String[2];
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Logger.d("OmSai ", "Single or Dula Sim " + manager.getPhoneCount());
                if(manager.getPhoneCount() ==2){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        imeiArray[0] =  manager.getImei(0);
                        imeiArray[1] =  manager.getImei(1);
                    } else {
                        imeiArray[0] =  manager.getDeviceId(0);
                        imeiArray[1] =  manager.getDeviceId(1);
                    }



                }
                else{
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        imeiArray[0] =  manager.getImei();
                        imeiArray[1] =  manager.getImei();
                    } else {
                        imeiArray[0] =manager.getDeviceId();
                        imeiArray[1] =manager.getDeviceId();
                    }


                }

            }
                return imeiArray;
        }

    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

    }

    public static void showMaterialDialog(Context context,String title, String description,  MaterialDialog.SingleButtonCallback okListener) {
        new MaterialStyledDialog.Builder(context)
                .setStyle(Style.HEADER_WITH_TITLE)
                .setTitle(title)
                .setHeaderColor(R.color.colorPrimary)
                .setDescription(description)
                .setPositiveText(context.getResources().getString(R.string.label_accept))
                .setNegativeText(context.getResources().getString(R.string.label_decline))
                .onNegative(okListener)
                .onPositive(okListener)
                .setScrollable(true)
                .setCancelable(false)
                .show();
    }

    public static void hideKeyboard(Activity mcontext){
        // Check if no view has focus:
        View view = mcontext.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)mcontext.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void startGpsService(Context mContext) {
        if (!CommonMethods.isServiceRunning(mContext, GpsTrackingService.class)) {
            Intent intent = new Intent(mContext, GpsTrackingService.class);
            mContext.startService(intent);
        }
    }
    public static boolean isServiceRunning(Context context, Class<?> clazz) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo serviceInfo : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (clazz.getName().equals(serviceInfo.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
