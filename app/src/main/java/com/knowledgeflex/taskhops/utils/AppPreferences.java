package com.knowledgeflex.taskhops.utils;

import android.content.Context;
import android.content.SharedPreferences;


public class AppPreferences {
    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_USER_TYPE = "user_type";
    public static final String KEY_UNIQUE_USERID = "uniqueuserid";
    public static final String KEY_ACCESS_TOKEN="access_token";
    public static final String KEY_SESSION_ID="session_id";






    private static final String APP_SHARED_PREFS	= AppPreferences.class.getSimpleName();
    private final Context mcontext;
    private SharedPreferences sharedPrefs;
    private SharedPreferences.Editor prefsEditor;
    public AppPreferences(Context context) {
        this.sharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS, Context.MODE_PRIVATE);
        this.prefsEditor = sharedPrefs.edit();
        this.mcontext = context;
    }
    public void clearall() {
        prefsEditor.clear();
        prefsEditor.commit();
    }

    public boolean isLoggedIn()
    {
        return sharedPrefs.getBoolean(IS_LOGIN, false);
    }

    public void saveIsLogin(boolean isLogin){
        prefsEditor.putBoolean(IS_LOGIN, isLogin);
        prefsEditor.commit();
    }
    public void saveUserType(String name){
        prefsEditor.putString(KEY_USER_TYPE,name);
        prefsEditor.commit();
    }
    public String getUserType(){
        return sharedPrefs.getString(KEY_USER_TYPE, "");
    }
    public void saveUserId(String userId){
        prefsEditor.putString(KEY_UNIQUE_USERID,userId);
        prefsEditor.commit();
    }
    public String getUserId(){
        return sharedPrefs.getString(KEY_UNIQUE_USERID, "");
    }
    public void saveAccessToken(String token){
        prefsEditor.putString(KEY_ACCESS_TOKEN,token);
        prefsEditor.commit();
    }


    public String getAccessToken(){
        return sharedPrefs.getString(KEY_ACCESS_TOKEN, "");
    }

    public void saveSessionId(String token){
        prefsEditor.putString(KEY_SESSION_ID,token);
        prefsEditor.commit();
    }


    public String getSessionId(){
        return sharedPrefs.getString(KEY_SESSION_ID, "");
    }
}
