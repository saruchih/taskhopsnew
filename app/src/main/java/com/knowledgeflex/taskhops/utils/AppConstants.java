package com.knowledgeflex.taskhops.utils;

/**
 * Created by saruchiarora on 26,November,2018
 */
public interface AppConstants {
    String DATABASE_NAME = "taskhops.db";
    String MOBILE_REGEX="^[7-9][0-9]{9}$";
    // ---------- Network Connection -----------------
    String NOINTERNET = "NOINTERNET";
    String INTERNETENABLED = "INTERNET ENABLED";
    int CODE_200=200;
    int CODE_202=202;
    int CODE_404=404;
    int CODE_417=417;

    String ACCOUNT = "TASKHOPS";

    // An account type, in the form of a domain name: If changed, change in stings.xml also
    String ACCOUNT_TYPE = "com.knowledgeflex.taskhops";

    // if changed, change in stings.xml also
    String AUTHORITY = "com.knowledgeflex.taskhops.android.syncadpter.provider";
    interface TableNames{
        String TABLE_LOGIN_SESSION ="login_session";
        String TABLE_USER_MASTER_ENTITY="user_master";
        String TABLE_AGENT_MASTER_ENTITY="agent_master";


    }

    interface Login{
        String KEY_MOBILENO="PhoneNo";
        String KEY_PASSWORD="Password";
        String KEY_IMEI_ONE="Imei";
        String KEY_IMEI_TWO="Imei1";

    }
    interface Signup{
        String KEY_EMAIL="Email";
        String KEY_CONTACTNO="ContactNo";
        String KEY_PASSWORD="Password";
        String KEY_IMEI_ONE="Imei";
        String KEY_IMEI_TWO="Imei1";
        String KEY_FIRST_NAME="FirstName";
        String KEY_LAST_NAME="LastName";
        String KEY_PASSCODE="PassCode";


    }
}
