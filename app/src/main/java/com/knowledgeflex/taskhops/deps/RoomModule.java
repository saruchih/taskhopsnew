package com.knowledgeflex.taskhops.deps;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.knowledgeflex.taskhops.database.local.TaskHopsDAO;
import com.knowledgeflex.taskhops.database.local.TaskHopsDb;
import com.knowledgeflex.taskhops.database.repository.LocalRepository;
import com.knowledgeflex.taskhops.database.repository.LocalRepositoryImp;
import com.knowledgeflex.taskhops.database.repository.RemoteRepository;
import com.knowledgeflex.taskhops.database.repository.RemoteRepositoryImp;
import com.knowledgeflex.taskhops.networking.NetworkService;
import com.knowledgeflex.taskhops.utils.AppConstants;
import java.util.concurrent.Executor;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;


@Module
public class RoomModule {

    private TaskHopsDb taskHopsDb;


    public RoomModule(Application mApplication) {
        taskHopsDb = Room.databaseBuilder(mApplication, TaskHopsDb.class, AppConstants.DATABASE_NAME).build();
    }

    @Singleton
    @Provides
    TaskHopsDb providesRoomDatabase() {
        return taskHopsDb;
    }

    @Singleton
    @Provides
    TaskHopsDAO providesIglDao(TaskHopsDb iglDatabase) {
        return iglDatabase.getTaskHops();
    }

    @Singleton
    @Provides
    public RemoteRepository getRemoteRepo(NetworkService networkService){
        return new RemoteRepositoryImp(networkService);
    }
    @Singleton
    @Provides
    public LocalRepository getLocalRepo(TaskHopsDAO taskHopsDAO, Executor exec){
        return new LocalRepositoryImp(taskHopsDAO, exec);
    }


}