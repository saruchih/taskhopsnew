
package com.knowledgeflex.taskhops.deps;

import android.app.Application;

import com.knowledgeflex.taskhops.ApplicationController;
import com.knowledgeflex.taskhops.component.home.HomeActivity;
import com.knowledgeflex.taskhops.component.login.LoginActivity;
import com.knowledgeflex.taskhops.component.login.forgotpassword.ForgotActivity;
import com.knowledgeflex.taskhops.component.signup.SignUpActivity;
import com.knowledgeflex.taskhops.component.splash.SplashActivity;
import com.knowledgeflex.taskhops.database.local.TaskHopsDAO;
import com.knowledgeflex.taskhops.database.local.TaskHopsDb;
import com.knowledgeflex.taskhops.database.repository.LocalRepositoryImp;
import com.knowledgeflex.taskhops.database.repository.RemoteRepositoryImp;
import com.knowledgeflex.taskhops.networking.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(dependencies = {}, modules = {AppModule.class, RoomModule.class, NetworkModule.class})
public interface AppComponent {

    void inject(ApplicationController applicationController);
    void inject(SplashActivity splashActivity);
    void inject(LoginActivity loginActivity);
    void inject(ForgotActivity forgotActivity);
    void inject(SignUpActivity signUpActivity);
    void inject(HomeActivity homeActivity);





    TaskHopsDAO taskHopsDao();
    TaskHopsDb TaskHopsDatabase();
    RemoteRepositoryImp getRemoteRepo();
    LocalRepositoryImp getLocalRepo();
    Application application();
    BaseUrlHolder provideBaseUrlHolder();
    Retrofit getRetrofit();
}