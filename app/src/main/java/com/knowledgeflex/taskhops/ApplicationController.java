package com.knowledgeflex.taskhops;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.knowledgeflex.taskhops.deps.AppComponent;
import com.knowledgeflex.taskhops.deps.AppModule;
import com.knowledgeflex.taskhops.deps.DaggerAppComponent;
import com.knowledgeflex.taskhops.deps.RoomModule;
import com.knowledgeflex.taskhops.networking.NetworkModule;
import com.knowledgeflex.taskhops.syncadapter.SyncAdapterInitialization;

import java.io.File;

/**
 * Created by saruchiarora on 26,November,2018
 */
public class ApplicationController extends Application{
    private static AppComponent appComponent;
    private  static SyncAdapterInitialization syncAdapterInstance;
    private static ApplicationController instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance= this;
        Stetho.initialize(Stetho.newInitializerBuilder(this)
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                .build());
        syncAdapterInstance = new SyncAdapterInitialization(this);
        initAppComponent();
        appComponent.inject(this);

    }

    public AppComponent initAppComponent()
    {
        File cacheFile = new File(getCacheDir(), "responses");

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .roomModule(new RoomModule(this))
                .networkModule(new NetworkModule(cacheFile))

                .build();

        return appComponent;
    }
    public static AppComponent getAppComponent()
    {
        return appComponent;
    }
    public static ApplicationController getInstance()
    {
        return instance;
    }
}
