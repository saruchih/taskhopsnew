package com.knowledgeflex.taskhops.base;

import android.arch.lifecycle.ViewModel;
import android.os.Bundle;

import com.knowledgeflex.taskhops.base.listner.BaseView;

import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by saruchiarora on 28,November,2018
 */

    public abstract class BaseViewModel<T> extends ViewModel{

        private WeakReference<T> view;

        protected AtomicBoolean isViewAlive = new AtomicBoolean();

        public T getView() {
            return view.get();
        }

        public void setView(T view) {
            this.view = new WeakReference<>(view);
        }

        public void initialize(Bundle extras) {
        }

        public void start() {
            isViewAlive.set(true);
        }

        public void finalizeView() {
            isViewAlive.set(false);
        }
    }

