package com.knowledgeflex.taskhops.base;

import android.app.AlertDialog;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import com.knowledgeflex.taskhops.R;
import com.knowledgeflex.taskhops.base.listner.ActionBarView;
import com.knowledgeflex.taskhops.networking.NetworkChangeReceiver;
import com.knowledgeflex.taskhops.utils.AppPreferences;
import com.knowledgeflex.taskhops.utils.ObjectUtil;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import dmax.dialog.SpotsDialog;


import static android.text.TextUtils.isEmpty;
import static com.knowledgeflex.taskhops.utils.ObjectUtil.isNull;


/**
 * saruchi
 */
public abstract class BaseActivity extends AppCompatActivity implements ActionBarView {
    public NetworkChangeReceiver networkCheck;
    public BaseViewModel baseViewModel;
    
    private Unbinder unbinder;
    protected Resources mResources;
    protected AppPreferences appPreferences;
    private AlertDialog spotsDialog;

    protected abstract void initializeDagger();
    protected abstract void initializeViewModel();
    public abstract int getLayoutId();
    protected abstract void setUpUi();
    ;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        unbinder = ButterKnife.bind(this);
        mResources= getResources();
        spotsDialog=  new SpotsDialog.Builder()
                .setContext(this)
                .setTheme(R.style.CustomProgressDialog)
                .build();
       

        appPreferences= new AppPreferences(this);
        initializeDagger();
        initializeViewModel();
        if (baseViewModel != null) {
            baseViewModel.initialize(getIntent().getExtras());
        }
        setUpUi();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (baseViewModel != null) {
            baseViewModel.start();
        }
    }

    @Override
    public void setUpIconVisibility(boolean visible) {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(visible);
        }
    }

    @Override
    public void setToolbarTitle(String titleKey) {
        final ActionBar actionBar = getSupportActionBar();
        if (!isNull(actionBar)) {
            if (!isEmpty(titleKey)) {
                actionBar.setTitle(titleKey);
            }
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onResume() {
       setNetworkCheck();
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (!ObjectUtil.isNull(networkCheck ))
                unregisterReceiver(networkCheck);
        } catch (Exception e) {

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (baseViewModel != null) {
            baseViewModel.finalizeView();
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    /**\
     * for network connection detection
     */
    private void setNetworkCheck(){
        try {
            networkCheck = new NetworkChangeReceiver(this);

            IntentFilter filter = new IntentFilter();
            filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
            registerReceiver(networkCheck, filter);

        } catch (Exception e) {
           // finish();
        }
    }
    public void snackBarWithMesg(View view, String mesg) {

       Snackbar mSnackbar = Snackbar.make(view, mesg, Snackbar.LENGTH_LONG);
        snackBarColor(mSnackbar);
        mSnackbar.show();
    }
    private void snackBarColor(Snackbar mSnackbar) {
        View view = mSnackbar.getView();
        mSnackbar.setActionTextColor(Color.WHITE);
        view.setBackgroundResource(R.color.colorPrimary);
    }

    public void showProgressDialog(String message){
        hideProgressDialog();
        spotsDialog.setMessage(message);
        spotsDialog.show();
    }

    public void hideProgressDialog(){
        if(!ObjectUtil.isNull(spotsDialog)){
            spotsDialog.dismiss();
        }
    }

}
