package com.knowledgeflex.taskhops.base.listner;

import android.view.View;

public interface OnRecyclerViewItemClickListener {

    void onRecyclerViewItemClick(View v, int position);
}