package com.knowledgeflex.taskhops.base.listner;


import com.knowledgeflex.taskhops.networking.NetworkError;

import retrofit2.Response;

/**
 * Created by saruchiarora on 1/20/18.
 */
public interface BaseCallback {
    void onSuccess(Response response);
    void onFail(NetworkError networkError);
}