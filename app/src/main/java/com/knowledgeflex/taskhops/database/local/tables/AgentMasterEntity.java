package com.knowledgeflex.taskhops.database.local.tables;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.knowledgeflex.taskhops.utils.AppConstants;

/**
 * Created by saruchiarora on 29,November,2018
 */
@Entity(tableName = AppConstants.TableNames.TABLE_AGENT_MASTER_ENTITY)
public class AgentMasterEntity {
    @PrimaryKey
    @NonNull
    private String agentId;
    private String agentName;
    private String vendorName;
    private String vendorId;
    private String firstName;
    private String middleName;
    private String lastName;
    private String companyName;
    private String email;
    private String contactNo;
    private String addressId;
    private String password;
    private String vendorIdMain;
    private String clientId;
    private String clientName;
    private String sessionId;



    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVendorIdMain() {
        return vendorIdMain;
    }

    public void setVendorIdMain(String vendorIdMain) {
        this.vendorIdMain = vendorIdMain;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String agentLoginId) {
        this.sessionId = agentLoginId;
    }
}
