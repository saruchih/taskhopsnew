package com.knowledgeflex.taskhops.database.local.tables;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.knowledgeflex.taskhops.utils.AppConstants;

/**
 * Created by Vinoth on 1/20/2017.
 */
@Entity(tableName = AppConstants.TableNames.TABLE_USER_MASTER_ENTITY)
public class UserMasterEntity {
    @PrimaryKey
    @NonNull
    private String userId;
   private String password;
   private String phone;
   private String email;
   private String updatedOn;
   private String complete;
   private String createOn;
   private String roleId;
   private String userGroupId;
   private String userName;
   private String status;
   private String imeiOne;
   private String imeiTwo;
   private String sessionId;
   private String companyName;

   @Ignore
   private String trackingType;
    @Ignore
    private String attendanceType;
    @Ignore
    private String checkingType;
    @Ignore
    private String disabledOn;



    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getComplete() {
        return complete;
    }

    public void setComplete(String complete) {
        this.complete = complete;
    }

    public String getCreateOn() {
        return createOn;
    }

    public void setCreateOn(String createOn) {
        this.createOn = createOn;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getUserGroupId() {
        return userGroupId;
    }

    public void setUserGroupId(String userGroupId) {
        this.userGroupId = userGroupId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImeiOne() {
        return imeiOne;
    }

    public void setImeiOne(String imeiOne) {
        this.imeiOne = imeiOne;
    }

    public String getImeiTwo() {
        return imeiTwo;
    }

    public void setImeiTwo(String imeiTwo) {
        this.imeiTwo = imeiTwo;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String agentLoginId) {
        this.sessionId = agentLoginId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTrackingType() {
        return trackingType;
    }

    public void setTrackingType(String trackingType) {
        this.trackingType = trackingType;
    }

    public String getAttendanceType() {
        return attendanceType;
    }

    public void setAttendanceType(String attendanceType) {
        this.attendanceType = attendanceType;
    }

    public String getCheckingType() {
        return checkingType;
    }

    public void setCheckingType(String checkingType) {
        this.checkingType = checkingType;
    }

    public String getDisabledOn() {
        return disabledOn;
    }

    public void setDisabledOn(String disabledOn) {
        this.disabledOn = disabledOn;
    }
}
