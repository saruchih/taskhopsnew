package com.knowledgeflex.taskhops.database.local.tables;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.knowledgeflex.taskhops.utils.AppConstants;


@Entity(tableName = AppConstants.TableNames.TABLE_LOGIN_SESSION)
public class LoginSessionEntityDummy {


    // inserting the two record for the two logined users rather than one .
    @PrimaryKey
    @NonNull
    @SerializedName("SessionId")
    @Expose
    private Integer sessionId;

    @NonNull
    public Integer getSessionId() {
        return sessionId;
    }

    public void setSessionId(@NonNull Integer sessionId) {
        this.sessionId = sessionId;
    }
}

