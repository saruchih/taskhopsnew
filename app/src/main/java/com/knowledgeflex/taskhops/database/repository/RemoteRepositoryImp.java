package com.knowledgeflex.taskhops.database.repository;

import com.knowledgeflex.taskhops.component.login.LoginResponse;
import com.knowledgeflex.taskhops.model.DataResponse;
import com.knowledgeflex.taskhops.networking.NetworkService;

import java.util.Map;

import javax.inject.Inject;

import retrofit2.Call;

/**
 * Created by saruchiarora on 26,November,2018
 */
public class RemoteRepositoryImp implements RemoteRepository {
    private NetworkService networkService;


    @Inject
    public RemoteRepositoryImp(NetworkService networkService) {
        this.networkService = networkService;
    }

    @Override
    public Call<LoginResponse> loginApiCall(Map<String, String> map) {
        return networkService.loginApiCall(map);
    }

    @Override
    public Call<DataResponse> logintokenApiCall(String userName, String password, String grantType) {
        return networkService.logintokenApiCall(userName,password,grantType);
    }

    @Override
    public Call<DataResponse> forgetpasswordApiCall(String emailid) {
        return networkService.forgetpasswordApiCall(emailid);
    }

    @Override
    public Call<DataResponse> signupApiCall(Map<String, String> map) {
        return networkService.signupApiCall(map);
    }

    @Override
    public Call<DataResponse> logoutApiCall(String id) {
        return networkService.logoutApiCall(id);
    }
}
