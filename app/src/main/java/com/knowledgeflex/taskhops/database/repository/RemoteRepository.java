package com.knowledgeflex.taskhops.database.repository;

import com.knowledgeflex.taskhops.component.login.LoginResponse;
import com.knowledgeflex.taskhops.model.DataResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.Query;

/**
 * Created by saruchiarora on 26,November,2018
 */
public interface RemoteRepository {
    Call<LoginResponse> loginApiCall(Map<String, String> map);
    Call<DataResponse> logintokenApiCall(String userName, String password, String grantType);
    Call<DataResponse> forgetpasswordApiCall (  String emailid);
    Call<DataResponse> signupApiCall(Map<String, String> map);
    Call<DataResponse> logoutApiCall( String id);


}
