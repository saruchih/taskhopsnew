package com.knowledgeflex.taskhops.database.local;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.Update;

import com.knowledgeflex.taskhops.database.local.tables.AgentMasterEntity;
import com.knowledgeflex.taskhops.database.local.tables.UserMasterEntity;
import com.knowledgeflex.taskhops.utils.AppConstants;

/**
 * Created by saruchiarora on 26,November,2018
 */
@Dao
public interface TaskHopsDAO {
    @Transaction
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertUserMasterRecord(UserMasterEntity userMasterEntity);

    @Transaction
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertAgentMasterRecord(AgentMasterEntity agentMasterEntity);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void updateUserMaster(UserMasterEntity userMasterEntity);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void updateAgentMasterEntity(AgentMasterEntity agentMasterEntity);

    @Transaction
    @Query("SELECT  * FROM "+ AppConstants.TableNames.TABLE_USER_MASTER_ENTITY +" where userId =:userid")
    LiveData<UserMasterEntity> getUserMasterRecord(String userid );
}
