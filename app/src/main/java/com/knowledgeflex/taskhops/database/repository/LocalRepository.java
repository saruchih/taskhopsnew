package com.knowledgeflex.taskhops.database.repository;

import android.arch.lifecycle.LiveData;

import com.knowledgeflex.taskhops.database.local.tables.AgentMasterEntity;
import com.knowledgeflex.taskhops.database.local.tables.UserMasterEntity;

/**
 * Created by saruchiarora on 26,November,2018
 */
public interface LocalRepository {
    void insertUserMasterEntity(UserMasterEntity userMasterEntity);
    void insertAgentMasterEntity(AgentMasterEntity agentMasterEntity);
    LiveData<UserMasterEntity> getUserMasterRecord(String userid );


}
