package com.knowledgeflex.taskhops.database.repository;

import android.arch.lifecycle.LiveData;

import com.knowledgeflex.taskhops.database.local.TaskHopsDAO;
import com.knowledgeflex.taskhops.database.local.tables.AgentMasterEntity;
import com.knowledgeflex.taskhops.database.local.tables.UserMasterEntity;

import java.util.concurrent.Executor;

import javax.inject.Inject;

/**
 * Created by saruchiarora on 26,November,2018
 */
public class LocalRepositoryImp implements LocalRepository {
    private TaskHopsDAO taskHopsDAO;
    private Executor executor;

    @Inject
    public LocalRepositoryImp(TaskHopsDAO cDAO, Executor exec) {
        taskHopsDAO = cDAO;
        executor = exec;
    }

    @Override
    public void insertUserMasterEntity(UserMasterEntity userMasterEntity) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                long id = taskHopsDAO.insertUserMasterRecord(userMasterEntity);
                if (id == -1) {
                    taskHopsDAO.updateUserMaster(userMasterEntity);
                }
            }
        });
    }

    @Override
    public void insertAgentMasterEntity(AgentMasterEntity agentMasterEntity) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                long id =  taskHopsDAO.insertAgentMasterRecord(agentMasterEntity);
                if (id == -1) {
                    taskHopsDAO.updateAgentMasterEntity(agentMasterEntity);
                }
            }
        });
    }

    @Override
    public LiveData<UserMasterEntity> getUserMasterRecord(String userid) {
        return taskHopsDAO.getUserMasterRecord(userid);
    }
}
