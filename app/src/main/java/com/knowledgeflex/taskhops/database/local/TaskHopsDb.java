package com.knowledgeflex.taskhops.database.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.knowledgeflex.taskhops.database.local.tables.AgentMasterEntity;
import com.knowledgeflex.taskhops.database.local.tables.LoginSessionEntityDummy;
import com.knowledgeflex.taskhops.database.local.tables.UserMasterEntity;
import com.knowledgeflex.taskhops.utils.Converters;

import static com.knowledgeflex.taskhops.database.local.TaskHopsDb.VERSION;

/**
 * Created by saruchiarora on 26,November,2018
 */
@Database(entities = {LoginSessionEntityDummy.class,
        UserMasterEntity.class,
        AgentMasterEntity.class

},version = VERSION)
@TypeConverters({Converters.class})  //convert ArrayList object to one of SQLite data type
public abstract class TaskHopsDb extends RoomDatabase {
    static final int VERSION = 1;
    public abstract TaskHopsDAO getTaskHops();
}
