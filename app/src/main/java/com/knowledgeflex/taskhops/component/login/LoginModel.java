package com.knowledgeflex.taskhops.component.login;

import com.knowledgeflex.taskhops.utils.ObjectUtil;

import javax.inject.Inject;

/**
 * Created by saruchiarora on 27,November,2018
 */
public class LoginModel {

    public String mobileNumber;
    public String password;
    public boolean isTermsAccepted;

    @Inject
    public LoginModel() {
    }

    public boolean isMobileValid(){
        if(ObjectUtil.isEmpty(mobileNumber)||mobileNumber.length()<10){
            return false;
        }
        else{
            return true;
        }
    }

    public boolean isPasswordValid(){
        if(ObjectUtil.isEmpty(password)){
            return false;
        }
        else{
            return true;
        }
    }
}
