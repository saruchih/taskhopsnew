package com.knowledgeflex.taskhops.component.home;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;
import com.knowledgeflex.taskhops.ApplicationController;
import com.knowledgeflex.taskhops.BuildConfig;
import com.knowledgeflex.taskhops.R;
import com.knowledgeflex.taskhops.base.BaseActivity;
import com.knowledgeflex.taskhops.component.login.LoginActivity;
import com.knowledgeflex.taskhops.component.signup.SignUpViewModel;
import com.knowledgeflex.taskhops.database.local.tables.UserMasterEntity;
import com.knowledgeflex.taskhops.gps.GpsTrackingService;
import com.knowledgeflex.taskhops.networking.NetworkUtil;
import com.knowledgeflex.taskhops.trueface.Trueface;
import com.knowledgeflex.taskhops.utils.CommonMethods;
import com.knowledgeflex.taskhops.utils.Logger;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.github.kbiakov.codeview.classifier.CodeProcessor;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import retrofit2.Response;

import static com.knowledgeflex.taskhops.utils.CommonMethods.showDialogOK;

public class HomeActivity extends BaseActivity implements HomeInteractor.View {
    private static final String TAG = HomeActivity.class.getSimpleName();
    private UserMasterEntity userMasterData;
    @BindView(R.id.imageView_logout)
    ImageView imageViewLogout;
    @BindView(R.id.text_username)
    TextView textUserName;
    @BindView(R.id.text_company)
    TextView textUserCompany;
    @BindView(R.id.text_version)
    TextView textversion;

    @Inject
    HomeViewModelFactory homeViewModelFactory;

    private HomeViewModel homeViewModel;
    private GpsTrackingService gpsTrackingService;

    @Override
    protected void initializeDagger() {
        ApplicationController.getAppComponent().inject(this);

    }

    @Override
    protected void initializeViewModel() {
        homeViewModel = ViewModelProviders.of(this, homeViewModelFactory).get(HomeViewModel.class);
        homeViewModel.setView(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_home;
    }
@OnClick(R.id.imageView_logout)
public void onLogoutClick(){
        logoutApiCall();
}
    @Override
    protected void setUpUi() {
        new Trueface.Services();
        CodeProcessor.init(this);
        CommonMethods commonMethods = new CommonMethods();
        commonMethods.startGpsService(this);
        gpsTrackingService = new GpsTrackingService();
        gpsTrackingService.startLocationUpdates();
        homeViewModel.getUserMasterRecord(appPreferences.getUserId()).observe(this, new Observer<UserMasterEntity>() {
            @Override
            public void onChanged(@Nullable UserMasterEntity userMasterEntity) {
                userMasterData = userMasterEntity;
                textUserName.setText(userMasterData.getUserName());
                textUserCompany.setText(userMasterData.getCompanyName());
                textversion.setText(BuildConfig.VERSION_NAME);
            }
        });




    }


    @Override
    public void navigateToMainScreen() {
        appPreferences.clearall();
        Intent intent = new Intent(this,  LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onSuccess(Response response) {

    }

    @Override
    public void onFailure(String appErrorMessage) {
        showToast(appErrorMessage);
    }

    @Override
    public void setLoaderVisibility(boolean isVisible) {
        if (isVisible) {
            showProgressDialog(mResources.getString(R.string.message_loading_please_wait));
        } else {
            hideProgressDialog();
        }
    }

    @Override
    public void logoutApiCall() {
        if(NetworkUtil.isConnected){
          promptUserLogout();
        }
        else
        {
            snackBarWithMesg(imageViewLogout,mResources.getString(R.string.error_no_internet));
        }
    }

    @Override
    public void showToast(String message) {
        CommonMethods.showToast(this,message);

    }

    /**
     *
     */
    private void getUserCurrentLocation() {
        setLoaderVisibility(true);
        SmartLocation.with(this).location()
                .oneFix()
                .start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        Logger.d(TAG, location.toString());
                        GpsTrackingService.setLocation(location);
                        setLoaderVisibility(false);


                    }
                });
    }

    private void promptUserLogout() {
        showDialogOK(this, mResources.getString(R.string.title_logout),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                homeViewModel.logoutApiCall(appPreferences.getSessionId());


                                break;
                            case DialogInterface.BUTTON_NEGATIVE:

                                finish();
                                break;
                        }
                    }
                });

    }


}

