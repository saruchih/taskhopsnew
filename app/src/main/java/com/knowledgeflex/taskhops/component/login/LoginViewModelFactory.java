package com.knowledgeflex.taskhops.component.login;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.knowledgeflex.taskhops.component.splash.SplashViewModel;
import com.knowledgeflex.taskhops.database.repository.LocalRepository;
import com.knowledgeflex.taskhops.database.repository.RemoteRepository;

import javax.inject.Inject;

/**
 * Created by saruchiarora on 27,November,2018
 */
public class LoginViewModelFactory implements ViewModelProvider.Factory {

    @Inject
    LocalRepository localRepository;
    @Inject
    RemoteRepository remoteRepository;


    @Inject
    public LoginViewModelFactory() {
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(LoginViewModel.class)) {
            return (T) new LoginViewModel(localRepository, remoteRepository);
        }
        throw new IllegalArgumentException("Wrong ViewModel class");
    }
}
