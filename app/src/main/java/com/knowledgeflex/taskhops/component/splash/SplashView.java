package com.knowledgeflex.taskhops.component.splash;

import android.arch.lifecycle.LiveData;

import com.knowledgeflex.taskhops.base.listner.BaseView;

/**
 * Created by saruchiarora on 26,November,2018
 */
public class SplashView {
    interface View extends BaseView {
        void NavigateToMainScreen();
        void onFailure(String appErrorMessage);
        void setLoaderVisibility(boolean isVisible);
    }
}
