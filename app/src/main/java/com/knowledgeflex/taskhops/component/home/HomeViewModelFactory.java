package com.knowledgeflex.taskhops.component.home;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.knowledgeflex.taskhops.component.signup.SignUpViewModel;
import com.knowledgeflex.taskhops.database.repository.LocalRepository;
import com.knowledgeflex.taskhops.database.repository.RemoteRepository;

import javax.inject.Inject;

/**
 * Created by saruchiarora on 30,November,2018
 */
public class HomeViewModelFactory implements ViewModelProvider.Factory {

    @Inject
    LocalRepository localRepository;
    @Inject
    RemoteRepository remoteRepository;


    @Inject
    public HomeViewModelFactory() {
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(HomeViewModel.class)) {
            return (T) new HomeViewModel(localRepository, remoteRepository);
        }
        throw new IllegalArgumentException("Wrong ViewModel class");
    }
}

