package com.knowledgeflex.taskhops.component.splash;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.knowledgeflex.taskhops.ApplicationController;
import com.knowledgeflex.taskhops.R;
import com.knowledgeflex.taskhops.base.BaseActivity;
import com.knowledgeflex.taskhops.component.home.HomeActivity;
import com.knowledgeflex.taskhops.component.login.LoginActivity;
import com.knowledgeflex.taskhops.utils.Logger;

public class SplashActivity extends BaseActivity implements SplashView.View  {
    private static final String TAG =  SplashActivity.class.getSimpleName();
    private static int SPLASH_TIME_OUT = 2000;

    @Override
    protected void initializeDagger() {
        ApplicationController.getAppComponent().inject(this);
        Logger.d(TAG+" onSuccess  ","getAppComponent");
    }

    @Override
    protected void initializeViewModel() {

    }


    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void setUpUi() {
        Thread timer = new Thread()
        {

            @Override
            public void run()

            {
                try {
                    sleep(SPLASH_TIME_OUT);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
                finally
                {
                    NavigateToMainScreen();

                    finish();
                }
            }
        };
        timer.start();
    }


    @Override
    public void NavigateToMainScreen() {
        if(appPreferences.isLoggedIn())   {
            Intent i=new Intent(SplashActivity.this,HomeActivity.class);
            startActivity(i);
            finish();
            //String usertype=isUserType();

//            if(usertype.equals("Standard User")) {
//                Intent intent = new Intent(_context, HomePage.class);
//                // Closing all the Activities
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                // Add new Flag to start new Activity
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                //Toast.makeText(getApplicationContext(),email, Toast.LENGTH_LONG).show();
//                _context.startActivity(intent);
//            }
//            else {
//
//                Intent intent = new Intent(_context, AgentActivity.class);
//                // Closing all the Activities
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                // Add new Flag to start new Activity
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                //Toast.makeText(getApplicationContext(),email, Toast.LENGTH_LONG).show();
//                _context.startActivity(intent);
//            }
        }
        else   {
            Intent i = new Intent(this, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }

    }

    @Override
    public void onFailure(String appErrorMessage) {

    }

    @Override
    public void setLoaderVisibility(boolean isVisible) {

    }
}
