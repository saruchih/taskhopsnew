package com.knowledgeflex.taskhops.component.login.forgotpassword;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.knowledgeflex.taskhops.ApplicationController;
import com.knowledgeflex.taskhops.R;
import com.knowledgeflex.taskhops.base.BaseActivity;
import com.knowledgeflex.taskhops.component.login.LoginActivity;
import com.knowledgeflex.taskhops.component.login.LoginViewModel;
import com.knowledgeflex.taskhops.component.login.LoginViewModelFactory;
import com.knowledgeflex.taskhops.networking.NetworkUtil;
import com.knowledgeflex.taskhops.utils.CommonMethods;
import com.knowledgeflex.taskhops.utils.ObjectUtil;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;

public class ForgotActivity extends BaseActivity implements ForgotPasswordInteractor.View{
    @BindView(R.id.edittext_email)
    EditText editTextMail;
    @BindView(R.id.button_send_email)
    Button btnEmail;

    @Inject
    ForgotViewModelFactory forgotViewModelFactory;

    private ForgotPasswordViewModel forgotPasswordViewModel;
    @Override
    protected void initializeDagger() {
        ApplicationController.getAppComponent().inject(this);

    }

    @Override
    protected void initializeViewModel() {
        forgotPasswordViewModel = ViewModelProviders.of(this, forgotViewModelFactory).get(ForgotPasswordViewModel.class);
        forgotPasswordViewModel.setView(this);
        setUpIconVisibility(true);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_forgot;
    }

    @Override
    protected void setUpUi() {

    }


    @Override
    public void navigateToMainScreen() {
        Intent i=new Intent(ForgotActivity.this,LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }

    @Override
    public void onSuccess(Response response) {
        if(response.code()==200){
            showToast(mResources.getString(R.string.messaage_email_sent));
            navigateToMainScreen();
        }
        else {
           showToast(mResources.getString(R.string.error_something_went_wrong));

        }

    }

    @OnClick(R.id.button_send_email)
    public void onSendEmailClick(){
        forgetApiCall();
    }

    @Override
    public void onFailure(String appErrorMessage) {
    showToast(appErrorMessage);
    }

    @Override
    public void setLoaderVisibility(boolean isVisible) {
        if(isVisible){
            showProgressDialog(mResources.getString(R.string.message_loading_please_wait));
        }
        else{
            hideProgressDialog();
        }
    }

    @Override
    public void forgetApiCall() {
        if(NetworkUtil.isConnected){
            validateData();
        }
        else
        {
            snackBarWithMesg(editTextMail,mResources.getString(R.string.error_no_internet));
        }
    }

    @Override
    public void showToast(String message) {
        CommonMethods.showToast(this,message);
    }

    private void validateData(){
        String email = editTextMail.getText().toString().trim();
        if(!ObjectUtil.isEmpty(email)
                && Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            forgotPasswordViewModel.forgotPasswordApiCall(email);

        }
        else{
            snackBarWithMesg(editTextMail,mResources.getString(R.string.error_valid_email_address));
        }
    }
}
