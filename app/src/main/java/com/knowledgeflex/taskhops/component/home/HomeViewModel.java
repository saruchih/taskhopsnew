package com.knowledgeflex.taskhops.component.home;

import android.arch.lifecycle.LiveData;

import com.knowledgeflex.taskhops.base.BaseViewModel;
import com.knowledgeflex.taskhops.component.signup.SignUpInteractor;
import com.knowledgeflex.taskhops.database.local.tables.UserMasterEntity;
import com.knowledgeflex.taskhops.database.repository.LocalRepository;
import com.knowledgeflex.taskhops.database.repository.RemoteRepository;
import com.knowledgeflex.taskhops.model.DataResponse;
import com.knowledgeflex.taskhops.networking.NetworkError;

import java.util.Map;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by saruchiarora on 30,November,2018
 */
public class HomeViewModel  extends BaseViewModel<HomeInteractor.View> {
    private LocalRepository localRepository;
    private RemoteRepository remoteRepository;
    @Inject
    public HomeViewModel(LocalRepository localRepository, RemoteRepository remoteRepository) {
        this.localRepository = localRepository;
        this.remoteRepository = remoteRepository;
    }

    public LiveData<UserMasterEntity> getUserMasterRecord(String userid) {
        return localRepository.getUserMasterRecord(userid);
    }

    public void logoutApiCall(String  userId){
        getView().setLoaderVisibility(true);
        Call<DataResponse> call = remoteRepository.logoutApiCall(userId);
        call.enqueue(new Callback<DataResponse>() {
            @Override
            public void onResponse(Call<DataResponse> call, Response<DataResponse> response) {
                getView().setLoaderVisibility(false);
                getView().onSuccess(response);

                switch (response.code()){
                    case 200 :
                        getView().navigateToMainScreen();
                        break;
                    case 404:
                        getView().showToast(response.body().getStatusMessage());
                        break;

                    case 202:
                        getView().showToast(response.body().getStatusMessage());

                        break;
                }


            }

            @Override
            public void onFailure(Call<DataResponse> call, Throwable t) {
                getView().setLoaderVisibility(false);
                getView().onFailure(new NetworkError(t).getAppErrorMessage());


            }
        });

    }
}

