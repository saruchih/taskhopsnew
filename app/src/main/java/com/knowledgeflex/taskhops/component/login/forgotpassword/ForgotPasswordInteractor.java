package com.knowledgeflex.taskhops.component.login.forgotpassword;

import com.knowledgeflex.taskhops.base.listner.BaseView;

import retrofit2.Response;

public class ForgotPasswordInteractor {

    interface View extends BaseView {
    void navigateToMainScreen();
    void onSuccess(Response response);
    void onFailure(String appErrorMessage);
    void setLoaderVisibility(boolean isVisible);
    void forgetApiCall();
    void showToast(String message);

}
}