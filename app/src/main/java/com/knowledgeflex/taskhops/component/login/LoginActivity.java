
package com.knowledgeflex.taskhops.component.login;

import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;
import com.knowledgeflex.taskhops.ApplicationController;
import com.knowledgeflex.taskhops.R;
import com.knowledgeflex.taskhops.base.BaseActivity;
import com.knowledgeflex.taskhops.base.listner.BaseCallback;
import com.knowledgeflex.taskhops.component.home.HomeActivity;
import com.knowledgeflex.taskhops.component.login.forgotpassword.ForgotActivity;
import com.knowledgeflex.taskhops.component.signup.SignUpActivity;
import com.knowledgeflex.taskhops.database.local.tables.AgentMasterEntity;
import com.knowledgeflex.taskhops.database.local.tables.UserMasterEntity;
import com.knowledgeflex.taskhops.networking.NetworkChangeReceiver;
import com.knowledgeflex.taskhops.networking.NetworkError;
import com.knowledgeflex.taskhops.networking.NetworkUtil;
import com.knowledgeflex.taskhops.utils.AppConstants;
import com.knowledgeflex.taskhops.utils.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;

import static com.knowledgeflex.taskhops.utils.CommonMethods.getImeiNumber;
import static com.knowledgeflex.taskhops.utils.CommonMethods.hideKeyboard;
import static com.knowledgeflex.taskhops.utils.CommonMethods.showDialogOK;
import static com.knowledgeflex.taskhops.utils.CommonMethods.showMaterialDialog;
import static com.knowledgeflex.taskhops.utils.CommonMethods.showToast;

public class LoginActivity extends BaseActivity implements LoginInteractor.View {
    private static final String TAG = LoginActivity.class.getSimpleName();
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    public static final String STANDARD_USER_TYPE="Standard User";
    @BindView(R.id.edit_mobile)
    EditText editMobile;
    @BindView(R.id.edit_password)
    EditText editPassword;
    @BindView(R.id.checkbox_termscondition)
    CheckBox checkBoxCondition;
    @BindView(R.id.button_signIn)
    Button btnSignIn;
    @BindView(R.id.textinput_layout_mobile)
    TextInputLayout textInputLayoutMobile;
    @BindView(R.id.textinput_layout_password)
    TextInputLayout textInputLayoutPaswsword;
    @BindView(R.id.textview_forgot_password)
    TextView textForgotPassword;
    @BindView(R.id.textview_signup)
    TextView textSignUp;

    @Inject
    LoginModel loginModel;

    @Inject
    LoginViewModelFactory loginViewModelFactory;

    private LoginViewModel loginViewModel;


    @Override
    protected void initializeDagger() {
        ApplicationController.getAppComponent().inject(this);
    }

    @Override
    protected void initializeViewModel() {
        loginViewModel = ViewModelProviders.of(this, loginViewModelFactory).get(LoginViewModel.class);
        loginViewModel.setView(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void setUpUi() {
        if(checkAndRequestPermissions()){

        }
        else{
           // setUpUi();
        }
        checkBoxCondition.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                new MaterialStyledDialog.Builder(LoginActivity.this)
                        .setStyle(Style.HEADER_WITH_TITLE)
                        .setTitle(mResources.getString(R.string.title_terms_conditions))
                        .setHeaderColor(R.color.colorPrimary)
                         .setDescription(mResources.getString(R.string.terms_conditions_description ))
                        .setPositiveText(getResources().getString(R.string.label_accept))
                        .setNegativeText(getResources().getString(R.string.label_decline))
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                loginModel.isTermsAccepted= false;
                                buttonView.setChecked(false);
                            }
                        })
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                loginModel.isTermsAccepted= true;
                                buttonView.setChecked(true);

                            }
                        })
                        .setScrollable(true)
                        .setCancelable(false)
                        .show();
            }
        });

    }

    @OnClick(R.id.textview_forgot_password)
    public void onForgotPasswordClick(){
        hideKeyboard(this);
        Intent i=new Intent(LoginActivity.this,ForgotActivity.class);
        startActivity(i);
        finish();
    }
    @OnClick(R.id.textview_signup)
    public void onSignUpClick(){
        hideKeyboard(this);
        Intent i=new Intent(LoginActivity.this,SignUpActivity.class);
        startActivity(i);
        finish();
    }
    @OnClick(R.id.button_signIn)
    public void onSignClick(){
        hideKeyboard(this);
        loginApiCall();
    }

    @Override
    public void navigateToMainScreen(String userType) {
        if(userType.equalsIgnoreCase(STANDARD_USER_TYPE)){
            hideKeyboard(this);
            Intent i=new Intent(LoginActivity.this,HomeActivity.class);
            startActivity(i);
            finish();

        }
        else{

        }

    }

    @Override
    public void onAccessTokenRecieved(String accessToken) {
        appPreferences.saveAccessToken(accessToken);
        appPreferences.saveIsLogin(true);

    }

    @Override
    public void onSuccess(Object object,String userType) {
        appPreferences.saveUserType(userType);
        Logger.d(TAG + " onSuccess", object.toString());
        if(userType.equalsIgnoreCase(STANDARD_USER_TYPE)){
            UserMasterEntity userMasterEntity = (UserMasterEntity)object;
            appPreferences.saveUserId(userMasterEntity.getUserId());
            appPreferences.saveSessionId(userMasterEntity.getSessionId());

        }
        else{
            AgentMasterEntity agentMasterEntity = (AgentMasterEntity)object;
            appPreferences.saveUserId(agentMasterEntity.getAgentId());
            appPreferences.saveSessionId(agentMasterEntity.getSessionId());

        }

    }

    @Override
    public void onFailure(String appErrorMessage) {
        snackBarWithMesg(checkBoxCondition,appErrorMessage);

    }

    @Override
    public void setLoaderVisibility(boolean isVisible) {
        if(isVisible){
            showProgressDialog(mResources.getString(R.string.message_loading_please_wait));
        }
        else{
            hideProgressDialog();
        }

    }

    @Override
    public void loginApiCall() {
        if(NetworkUtil.isConnected){
            validateData();
        }
        else
            {
            snackBarWithMesg(checkBoxCondition,mResources.getString(R.string.error_no_internet));
        }
    }

    private void validateData(){
        loginModel.mobileNumber = editMobile.getText().toString().trim();
        loginModel.password = editPassword.getText().toString().trim();
        loginModel.isTermsAccepted = checkBoxCondition.isChecked();
        if(!loginModel.isMobileValid()){
            textInputLayoutMobile.setError(mResources.getString(R.string.error_valid_mobile_number));
        }
        else if (!loginModel.isPasswordValid()){
            textInputLayoutPaswsword.setError(mResources.getString(R.string.error_valid_password));
        }
        else if(!loginModel.isTermsAccepted){
            snackBarWithMesg(checkBoxCondition,mResources.getString(R.string.error_accept_terms_condition));
        }
        else {
            Map<String, String> map = new HashMap<>();
            map.put(AppConstants.Login.KEY_MOBILENO, loginModel.mobileNumber);
            map.put(AppConstants.Login.KEY_PASSWORD, loginModel.password);
            map.put(AppConstants.Login.KEY_IMEI_ONE, getImeiNumber(this)[0]);
            map.put(AppConstants.Login.KEY_IMEI_TWO, getImeiNumber(this)[1]);

            Logger.d(TAG + " login map ", map.toString());
            loginViewModel.callLoginApi(map);
        }
    }
    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        int folderPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int folderPermission1 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int phonePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        int cameraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int audioPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        int readPermission = ContextCompat.checkSelfPermission(this,Manifest.permission.READ_SMS);
        int receivePermission = ContextCompat.checkSelfPermission(this,Manifest.permission.RECEIVE_SMS);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (folderPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (folderPermission1 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (phonePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CALL_PHONE);

        }
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);

        }
        if (audioPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECORD_AUDIO);

        }
        if (receivePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECEIVE_SMS);

        }
        if (readPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);

        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CALL_PHONE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.RECEIVE_SMS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_SMS, PackageManager.PERMISSION_GRANTED);
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    if (perms.get(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED &&
                            perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                            perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                            perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                            perms.get(Manifest.permission.CALL_PHONE) ==
                                    PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.CAMERA) ==
                            PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.RECORD_AUDIO) ==
                            PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.RECEIVE_SMS) ==
                            PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {

                        //xsetUpUi();
                    }
                    else {

                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE) ||
                                ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION) ||
                                ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                                ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||

                                ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE) ||
                                ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                                ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO) ||
                                ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECEIVE_SMS) ||
                                ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_SMS)) {
                            showDialogOK(this,mResources.getString(R.string.title_permission),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case DialogInterface.BUTTON_POSITIVE:
                                        checkAndRequestPermissions();
                                        break;
                                    case DialogInterface.BUTTON_NEGATIVE:

                                        finish();
                                        break;
                                }
                            }
                        });
                        }
                    }
                }
            }
        }
    }



}
