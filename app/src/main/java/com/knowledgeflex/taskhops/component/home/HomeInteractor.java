package com.knowledgeflex.taskhops.component.home;

import com.knowledgeflex.taskhops.base.listner.BaseView;

import retrofit2.Response;

/**
 * Created by saruchiarora on 30,November,2018
 */
public class HomeInteractor {
    interface View extends BaseView {
        void navigateToMainScreen();
        void onSuccess(Response response);
        void onFailure(String appErrorMessage);
        void setLoaderVisibility(boolean isVisible);
        void logoutApiCall();
        void showToast(String message);

    }
}
