package com.knowledgeflex.taskhops.component.signup;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.knowledgeflex.taskhops.ApplicationController;
import com.knowledgeflex.taskhops.R;
import com.knowledgeflex.taskhops.base.BaseActivity;
import com.knowledgeflex.taskhops.component.login.LoginActivity;
import com.knowledgeflex.taskhops.component.login.forgotpassword.ForgotPasswordViewModel;
import com.knowledgeflex.taskhops.networking.NetworkUtil;
import com.knowledgeflex.taskhops.utils.AppConstants;
import com.knowledgeflex.taskhops.utils.CommonMethods;
import com.knowledgeflex.taskhops.utils.Logger;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;

import static com.knowledgeflex.taskhops.utils.CommonMethods.getImeiNumber;

public class SignUpActivity extends BaseActivity implements SignUpInteractor.View {
    private static final String TAG = SignUpActivity.class.getSimpleName();
    @BindView(R.id.text_input_first_name)
    TextInputLayout textInputLayoutFirstName;
    @BindView(R.id.edit_firstman)
    EditText editFirstName;
    @BindView(R.id.input_layout_lastname)
    TextInputLayout textInputLayoutLastname;
    @BindView(R.id.edit_lastname)
    EditText editLastName;
    @BindView(R.id.input_layout_mobile)
    TextInputLayout textInputLayoutMobile;
    @BindView(R.id.edit_mobile)
    EditText editMobile;
    @BindView(R.id.input_layout_password)
    TextInputLayout textInputLayoutPassword;
    @BindView(R.id.edit_password)
    EditText editPassword;
    @BindView(R.id.input_layout_email)
    TextInputLayout textInputLayoutEmail;
    @BindView(R.id.edit_email)
    EditText editEmail;
    @BindView(R.id.input_layout_passcord)
    TextInputLayout textInputLayoutPasscode;
    @BindView(R.id.edit_passcord)
    EditText editPasscord;
    @BindView(R.id.textview_terms_conditions)
    TextView textTermsConditions;
    @BindView(R.id.btn_signup)
    Button buttonSignUp
            ;
    private SignUpModel signUpModel;

    @Inject
    SignUpViewModelFactory signUpViewModelFactory;

    private SignUpViewModel signUpViewModel;

    @Override
    protected void initializeDagger() {
        ApplicationController.getAppComponent().inject(this);
    }

    @Override
    protected void initializeViewModel() {
        signUpViewModel = ViewModelProviders.of(this, signUpViewModelFactory).get(SignUpViewModel.class);
        signUpViewModel.setView(this);
        setUpIconVisibility(true);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_sign_up;
    }

    @Override
    protected void setUpUi() {
        signUpModel = new SignUpModel();
         String termsText = "By SignUp, you are agree to our";
        String termsLink = " <a href=http://taskhops.com/PrivacyPolicy.html >Terms of Service</a>";
        String privacyLink = " and our <a href=http://taskhops.com/PrivacyPolicy.html  >Privacy Policy.</a>";
        String allText = termsText + termsLink + privacyLink;

        textTermsConditions.setClickable(true);
        textTermsConditions.setMovementMethod(LinkMovementMethod.getInstance());
        textTermsConditions.setText(Html.fromHtml(allText));

    }


    @Override
    public void navigateToMainScreen() {
        showToast(mResources.getString(R.string.messaage_signup_sucessfully));
        Intent i=new Intent(SignUpActivity.this,LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }

    @OnClick(R.id.btn_signup)
    public void onSignUpClick(){
        signUpApiCall();
    }
    @Override
    public void onSuccess(Response response) {

    }

    @Override
    public void onFailure(String appErrorMessage) {
        showToast(appErrorMessage);
    }

    @Override
    public void setLoaderVisibility(boolean isVisible) {
        if(isVisible){
            showProgressDialog(mResources.getString(R.string.message_loading_please_wait));
        }
        else{
            hideProgressDialog();
        }
    }

    @Override
    public void signUpApiCall() {
        if(NetworkUtil.isConnected){
            validateData();
        }
        else
        {
            snackBarWithMesg(editFirstName,mResources.getString(R.string.error_no_internet));
        }

    }

    @Override
    public void showToast(String message) {
        CommonMethods.showToast(this,message);

    }

    private void validateData(){
        signUpModel.firstName = editFirstName.getText().toString().trim();
        signUpModel.lastName = editLastName.getText().toString().trim();
        signUpModel.mobileNo = editMobile.getText().toString().trim();
        signUpModel.password = editPassword.getText().toString().trim();
        signUpModel.emailId = editMobile.getText().toString().trim();
        signUpModel.passcode = editPasscord.getText().toString().trim();
        if(signUpModel.fieldValidation(signUpModel.firstName)){
            textInputLayoutFirstName.setError(mResources.getString(R.string.error_enter_first_name));

        }

        else if (!signUpModel.fieldValidation(signUpModel.lastName)){
            textInputLayoutLastname.setError(mResources.getString(R.string.error_enter_last_name));

        }
        else if (!signUpModel.fieldValidation(signUpModel.mobileNo)|| !signUpModel.isMobileValid()){
            textInputLayoutMobile.setError(mResources.getString(R.string.error_valid_mobile_number));


        }
        else if (!signUpModel.fieldValidation(signUpModel.password)|| !signUpModel.ispassworddValid()){
            //textInputLayoutPassword.setError(mResources.getString(R.string.error_signup_passowrd_valid));
            showToast(mResources.getString(R.string.error_signup_passowrd_valid));

        }
         else if (signUpModel.fieldValidation(signUpModel.emailId) || !signUpModel.isEmailValid()){
            //textInputLayoutEmail.setError(mResources.getString(R.string.error_valid_email_address));
            showToast(mResources.getString(R.string.error_valid_email_address));



        }
        else if (signUpModel.fieldValidation(signUpModel.passcode)){
           // textInputLayoutPasscode.setError(mResources.getString(R.string.error_valid_passcode));
            showToast(mResources.getString(R.string.error_valid_email_address));

        }
        else{
            Map<String, String> map = new HashMap<>();
            map.put(AppConstants.Signup.KEY_EMAIL, signUpModel.emailId);
            map.put(AppConstants.Signup.KEY_FIRST_NAME, signUpModel.firstName);
            map.put(AppConstants.Signup.KEY_LAST_NAME, signUpModel.lastName);
            map.put(AppConstants.Signup.KEY_CONTACTNO, signUpModel.mobileNo);
            map.put(AppConstants.Signup.KEY_PASSWORD, signUpModel.password);
            map.put(AppConstants.Signup.KEY_IMEI_ONE, getImeiNumber(this)[0]);
            map.put(AppConstants.Signup.KEY_IMEI_TWO, getImeiNumber(this)[1]);
            map.put(AppConstants.Signup.KEY_PASSCODE, signUpModel.passcode);
            signUpViewModel.signUpApiCall(map);


            Logger.d(TAG + " login map ", map.toString());

        }
    }
}
