package com.knowledgeflex.taskhops.component.login;

import com.knowledgeflex.taskhops.base.listner.BaseView;

import retrofit2.Response;

/**
 * Created by saruchiarora on 27,November,2018
 */
public class LoginInteractor {

    interface View extends BaseView {
    void navigateToMainScreen(String userType);
    void onAccessTokenRecieved(String accessToken);
    void onSuccess(Object response,String userType);
    void onFailure(String appErrorMessage);
    void setLoaderVisibility(boolean isVisible);
    void loginApiCall();
}
}