package com.knowledgeflex.taskhops.component.signup;


import com.knowledgeflex.taskhops.base.BaseViewModel;
import com.knowledgeflex.taskhops.database.repository.LocalRepository;
import com.knowledgeflex.taskhops.database.repository.RemoteRepository;
import com.knowledgeflex.taskhops.model.DataResponse;
import com.knowledgeflex.taskhops.networking.NetworkError;

import java.util.Map;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by saruchiarora on 30,November,2018
 */
public class SignUpViewModel  extends BaseViewModel<SignUpInteractor.View> {
    private LocalRepository localRepository;
    private RemoteRepository remoteRepository;
    @Inject
    public SignUpViewModel(LocalRepository localRepository, RemoteRepository remoteRepository) {
        this.localRepository = localRepository;
        this.remoteRepository = remoteRepository;
    }

    public void signUpApiCall(Map<String, String> map){
        getView().setLoaderVisibility(true);
        Call<DataResponse> call = remoteRepository.signupApiCall(map);
        call.enqueue(new Callback<DataResponse>() {
            @Override
            public void onResponse(Call<DataResponse> call, Response<DataResponse> response) {
                getView().setLoaderVisibility(false);
                getView().onSuccess(response);

                switch (response.code()){
                    case 200 :
                        getView().navigateToMainScreen();
                        break;
                    case 404:
                        getView().showToast(response.body().getStatusMessage());
                       break;

                    case 202:
                        getView().showToast(response.body().getStatusMessage());

                        break;
                }


            }

            @Override
            public void onFailure(Call<DataResponse> call, Throwable t) {
                getView().setLoaderVisibility(false);
                getView().onFailure(new NetworkError(t).getAppErrorMessage());


            }
        });

    }
}
