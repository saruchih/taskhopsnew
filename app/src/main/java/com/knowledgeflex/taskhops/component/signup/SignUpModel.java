package com.knowledgeflex.taskhops.component.signup;

import android.util.Patterns;

import com.knowledgeflex.taskhops.utils.ObjectUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by saruchiarora on 30,November,2018
 */
public class SignUpModel {
    public  String regex = "((?=.*[0-9])(?=.*[a-zA-Z]))";
    public String firstName;
    public String lastName;
    public String mobileNo;
    public String password;
    public String emailId;
    public String passcode;

    public boolean fieldValidation(String field){
        if(ObjectUtil.isEmpty(field)){
            return false;
        }
        else{
            return true;
        }

    }
    public boolean isMobileValid(){
        if(ObjectUtil.isEmpty(mobileNo)||mobileNo.length()<10){
            return false;
        }
        else{
            return true;
        }
    }

    public boolean ispassworddValid(){
        Pattern pattern=Pattern.compile(regex);
        Matcher matcher=pattern.matcher(password);
        if(!matcher.find() ||
                password.length()<8) {
            return false;
        }
        else{
            return true;
        }

    }

    public boolean isEmailValid(){
        if(!Patterns.EMAIL_ADDRESS.matcher(emailId).matches()){
            return false;
        }
        else{
            return true;
        }
    }
}
