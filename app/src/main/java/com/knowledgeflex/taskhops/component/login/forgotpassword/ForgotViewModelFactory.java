package com.knowledgeflex.taskhops.component.login.forgotpassword;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.knowledgeflex.taskhops.database.repository.LocalRepository;
import com.knowledgeflex.taskhops.database.repository.RemoteRepository;

import javax.inject.Inject;

public class ForgotViewModelFactory implements ViewModelProvider.Factory {

    @Inject
    LocalRepository localRepository;
    @Inject
    RemoteRepository remoteRepository;


    @Inject
    public ForgotViewModelFactory() {
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(ForgotPasswordViewModel.class)) {
            return (T) new ForgotPasswordViewModel(localRepository, remoteRepository);
        }
        throw new IllegalArgumentException("Wrong ViewModel class");
    }
}
