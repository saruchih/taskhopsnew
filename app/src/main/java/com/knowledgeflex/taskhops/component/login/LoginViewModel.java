package com.knowledgeflex.taskhops.component.login;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Intent;
import android.util.Log;

import com.knowledgeflex.taskhops.R;
import com.knowledgeflex.taskhops.base.BaseViewModel;
import com.knowledgeflex.taskhops.base.listner.BaseCallback;
import com.knowledgeflex.taskhops.database.local.tables.AgentMasterEntity;
import com.knowledgeflex.taskhops.database.local.tables.UserMasterEntity;
import com.knowledgeflex.taskhops.database.repository.LocalRepository;
import com.knowledgeflex.taskhops.database.repository.RemoteRepository;
import com.knowledgeflex.taskhops.model.DataResponse;
import com.knowledgeflex.taskhops.networking.NetworkError;
import com.knowledgeflex.taskhops.utils.AppConstants;
import com.knowledgeflex.taskhops.utils.Logger;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Map;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by saruchiarora on 27,November,2018
 */
public class LoginViewModel extends BaseViewModel<LoginInteractor.View> {
    private  static final String TAG = LoginViewModel.class.getSimpleName();
    private static final String GRAND_TYPE="password";
    private LocalRepository localRepository;
    private RemoteRepository remoteRepository;
    @Inject
    public LoginViewModel(LocalRepository localRepository, RemoteRepository remoteRepository) {
    this.localRepository = localRepository;
    this.remoteRepository = remoteRepository;
    }



    protected void callLoginApi(Map<String, String> map){
        getView().setLoaderVisibility(true);
        Call<LoginResponse> call = remoteRepository.loginApiCall(map);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                getView().setLoaderVisibility(false);
                parseResponse(response,map);

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                getView().setLoaderVisibility(false);
                getView().onFailure(new NetworkError(t).getAppErrorMessage());


            }
        });

    }

    private void parseResponse(Response response,Map<String, String> map){
        try {
            JSONArray jsonArray = new JSONArray(((LoginResponse) response.body()).getData());
            for(int i=0;i<jsonArray.length();i++) {
                if (jsonArray.getJSONObject(i).getString("UserType").equals("Standard User")){
                    UserMasterEntity userMasterEntity = new UserMasterEntity();
                    userMasterEntity.setCompanyName(jsonArray.getJSONObject(i).getString("ClientName"));
                    userMasterEntity.setPassword(jsonArray.getJSONObject(i).getString("Password"));
                    userMasterEntity.setUserId(jsonArray.getJSONObject(i).getString("id"));
                    userMasterEntity.setEmail(jsonArray.getJSONObject(i).getString("Email"));
                    userMasterEntity.setUserName(jsonArray.getJSONObject(i).getString("UserName"));
                    userMasterEntity.setPhone(jsonArray.getJSONObject(i).getString("Phone"));
                    userMasterEntity.setImeiOne(jsonArray.getJSONObject(i).getString("Imei"));
                    userMasterEntity.setImeiTwo(jsonArray.getJSONObject(i).getString("Imei1"));
                    userMasterEntity.setStatus(jsonArray.getJSONObject(i).getString("Status"));
                    userMasterEntity.setTrackingType(jsonArray.getJSONObject(i).getString("TrackType"));
                    userMasterEntity.setAttendanceType(jsonArray.getJSONObject(i).getString("AttendanceType"));
                    userMasterEntity.setCheckingType(jsonArray.getJSONObject(i).getString("CheckingType"));
                    userMasterEntity.setCreateOn(jsonArray.getJSONObject(i).getString("createdAt"));
                    userMasterEntity.setUpdatedOn(jsonArray.getJSONObject(i).getString("updatedAt"));
                    userMasterEntity.setDisabledOn(jsonArray.getJSONObject(i).getString("DisabledOn"));
                    userMasterEntity.setRoleId(jsonArray.getJSONObject(i).getString("RoleId"));
                    userMasterEntity.setUserGroupId(jsonArray.getJSONObject(i).getString("UserGroupId"));
                    userMasterEntity.setSessionId(jsonArray.getJSONObject(i).getString("sid"));
                    localRepository.insertUserMasterEntity(userMasterEntity);
                    getView().onSuccess(userMasterEntity,jsonArray.getJSONObject(i).getString("UserType"));

                }
                else{
                    AgentMasterEntity agentMasterEntity = new AgentMasterEntity();
                    agentMasterEntity.setAgentId(jsonArray.getJSONObject(i).getString("AgentId"));
                    agentMasterEntity.setAgentName(jsonArray.getJSONObject(i).getString("AgentName"));
                    agentMasterEntity.setVendorName(jsonArray.getJSONObject(i).getString("VendorName"));
                    //agentMasterEntity.setAgentId1(jsonArray.getJSONObject(i).getString("Id"));
                    agentMasterEntity.setVendorId(jsonArray.getJSONObject(i).getString("VendorId"));
                    agentMasterEntity.setFirstName(jsonArray.getJSONObject(i).getString("FirstName"));
                    agentMasterEntity.setMiddleName(jsonArray.getJSONObject(i).getString("MiddleName"));
                    agentMasterEntity.setLastName(jsonArray.getJSONObject(i).getString("LastName"));
                    agentMasterEntity.setCompanyName(jsonArray.getJSONObject(i).getString("CompanyName"));
                    agentMasterEntity.setEmail(jsonArray.getJSONObject(i).getString("Email"));
                    agentMasterEntity.setContactNo(jsonArray.getJSONObject(i).getString("ContactNo"));
                    agentMasterEntity.setAddressId(jsonArray.getJSONObject(i).getString("AddressId"));
                    agentMasterEntity.setPassword(jsonArray.getJSONObject(i).getString("Password"));
                    agentMasterEntity.setVendorIdMain(jsonArray.getJSONObject(i).getString("VendorIdMain"));
                    agentMasterEntity.setClientId(jsonArray.getJSONObject(i).getString("ClientId"));
                    agentMasterEntity.setClientName(jsonArray.getJSONObject(i).getString("ClientName"));
                    agentMasterEntity.setSessionId(jsonArray.getJSONObject(i).getString("sid"));
                    localRepository.insertAgentMasterEntity(agentMasterEntity);
                    getView().onSuccess(agentMasterEntity,jsonArray.getJSONObject(i).getString("UserType"));

                }
                callLoginTokenApi(jsonArray.getJSONObject(i).getString("UserType"),map);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void callLoginTokenApi(String userType,Map<String, String> map){
        if (map.containsKey(AppConstants.Login.KEY_MOBILENO) &&
                map.containsKey(AppConstants.Login.KEY_PASSWORD) ) {
            String  mobileNo = map.get(AppConstants.Login.KEY_MOBILENO);
            String  password = map.get(AppConstants.Login.KEY_PASSWORD);

        getView().setLoaderVisibility(true);
        Call<DataResponse> call = remoteRepository.logintokenApiCall(mobileNo,password,GRAND_TYPE);
        call.enqueue(new Callback<DataResponse>() {
            @Override
            public void onResponse(Call<DataResponse> call, Response<DataResponse> response) {
                getView().setLoaderVisibility(false);

                try {
                    if (response.isSuccessful()) {
                       String token = response.body().getAccess_token();
                        String type = response.body().getToken_type();
                        String expire = response.body().getExpire();
                        String accesstoken = type + " " + token;
                        getView().onAccessTokenRecieved(accesstoken);
                        getView().navigateToMainScreen(userType);
                    }


                }
                catch(Exception e){
                   e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<DataResponse> call, Throwable t) {
                getView().setLoaderVisibility(false);
                getView().onFailure(new NetworkError(t).getAppErrorMessage());
            }
        });
        }
        else{
            Logger.d(TAG, "no mobile number and password");
        }
    }
}
