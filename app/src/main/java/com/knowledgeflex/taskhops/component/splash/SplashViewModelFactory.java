package com.knowledgeflex.taskhops.component.splash;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.knowledgeflex.taskhops.database.repository.LocalRepository;
import com.knowledgeflex.taskhops.database.repository.RemoteRepository;

import javax.inject.Inject;

public class SplashViewModelFactory implements ViewModelProvider.Factory {

    @Inject
    LocalRepository localRepository;
    @Inject
    RemoteRepository remoteRepository;


    @Inject
    public SplashViewModelFactory() {
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(SplashViewModel.class)) {
            return (T) new SplashViewModel(localRepository, remoteRepository);
        }
        throw new IllegalArgumentException("Wrong ViewModel class");
    }
}