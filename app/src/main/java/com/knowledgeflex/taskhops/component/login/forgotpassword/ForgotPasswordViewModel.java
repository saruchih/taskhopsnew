package com.knowledgeflex.taskhops.component.login.forgotpassword;

import com.knowledgeflex.taskhops.base.BaseViewModel;
import com.knowledgeflex.taskhops.component.login.LoginResponse;
import com.knowledgeflex.taskhops.database.repository.LocalRepository;
import com.knowledgeflex.taskhops.database.repository.RemoteRepository;
import com.knowledgeflex.taskhops.model.DataResponse;
import com.knowledgeflex.taskhops.networking.NetworkError;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by saruchiarora on 28,November,2018
 */
public class ForgotPasswordViewModel extends BaseViewModel<ForgotPasswordInteractor.View> {
    private LocalRepository localRepository;
    private RemoteRepository remoteRepository;
    @Inject
    public ForgotPasswordViewModel(LocalRepository localRepository, RemoteRepository remoteRepository) {
        this.localRepository = localRepository;
        this.remoteRepository = remoteRepository;
    }

    public void forgotPasswordApiCall(String emailAddress){
        getView().setLoaderVisibility(true);
        Call<DataResponse> call = remoteRepository.forgetpasswordApiCall(emailAddress);
        call.enqueue(new Callback<DataResponse>() {
            @Override
            public void onResponse(Call<DataResponse> call, Response<DataResponse> response) {
                getView().setLoaderVisibility(false);
                getView().onSuccess(response);



            }

            @Override
            public void onFailure(Call<DataResponse> call, Throwable t) {
                getView().setLoaderVisibility(false);
                getView().onFailure(new NetworkError(t).getAppErrorMessage());


            }
        });

    }
}
