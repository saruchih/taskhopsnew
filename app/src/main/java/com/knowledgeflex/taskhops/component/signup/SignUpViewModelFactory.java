package com.knowledgeflex.taskhops.component.signup;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.knowledgeflex.taskhops.component.splash.SplashViewModel;
import com.knowledgeflex.taskhops.database.repository.LocalRepository;
import com.knowledgeflex.taskhops.database.repository.RemoteRepository;

import javax.inject.Inject;

/**
 * Created by saruchiarora on 30,November,2018
 */
public class SignUpViewModelFactory implements ViewModelProvider.Factory {

    @Inject
    LocalRepository localRepository;
    @Inject
    RemoteRepository remoteRepository;


    @Inject
    public SignUpViewModelFactory() {
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(SignUpViewModel.class)) {
            return (T) new SignUpViewModel(localRepository, remoteRepository);
        }
        throw new IllegalArgumentException("Wrong ViewModel class");
    }
}
