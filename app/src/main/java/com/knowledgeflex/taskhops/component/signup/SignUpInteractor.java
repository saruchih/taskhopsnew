package com.knowledgeflex.taskhops.component.signup;

import com.knowledgeflex.taskhops.base.listner.BaseView;

import retrofit2.Response;

/**
 * Created by saruchiarora on 30,November,2018
 */
public class SignUpInteractor {
    interface View extends BaseView {
        void navigateToMainScreen();
        void onSuccess(Response response);
        void onFailure(String appErrorMessage);
        void setLoaderVisibility(boolean isVisible);
        void signUpApiCall();
        void showToast(String message);

    }
}
