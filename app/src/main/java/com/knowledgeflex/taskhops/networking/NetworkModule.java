package com.knowledgeflex.taskhops.networking;



import com.knowledgeflex.taskhops.BuildConfig;
import com.knowledgeflex.taskhops.deps.BaseUrlHolder;
import com.knowledgeflex.taskhops.utils.Logger;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {
    File cacheFile;
    private static final int TIMEOUT_IN_MS = 30000;

    public NetworkModule(File cacheFile) {
        this.cacheFile = cacheFile;
    }
    @Singleton
    @Provides
    public Executor getExecutor(){
        return  Executors.newFixedThreadPool(4);
    }
    @Provides
    @Singleton
    Retrofit provideCall(BaseUrlHolder baseUrlHolder) {
        Cache cache = null;
        try {

            cache = new Cache(cacheFile, 10 * 1024 * 1024);
        } catch (Exception e) {
            e.printStackTrace();
        }
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(logging)
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();

                        // Customize the request
                        Request request = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .removeHeader("Pragma")
                                .header("Cache-Control", String.format("max-age=%d", BuildConfig.CACHETIME))
                                .build();

                        okhttp3.Response response = chain.proceed(request);
                        response.cacheResponse();

                        // Customize or return the response
                        return response;
                    }
                })
                .addInterceptor(logging)
                .cache(cache)
                .connectTimeout(TIMEOUT_IN_MS, TimeUnit.MILLISECONDS)
                .build();
        Logger.d("Network Module",baseUrlHolder.getBaseUrl() );
        return new Retrofit.Builder()
                .baseUrl(baseUrlHolder.getBaseUrl())
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                //.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public NetworkService providesNetworkService(
             Retrofit retrofit) {
        return retrofit.create(NetworkService.class);
    }

    //@AppScope
    @Provides
    BaseUrlHolder provideBaseUrlHolder() {
        return new BaseUrlHolder(BuildConfig.BASEURL);
    }
}
