package com.knowledgeflex.taskhops.networking;


import com.knowledgeflex.taskhops.component.login.LoginResponse;
import com.knowledgeflex.taskhops.model.DataResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface NetworkService {


    @FormUrlEncoded
    @POST("MobileLogin")
    Call<LoginResponse> loginApiCall(@FieldMap Map<String, String> map);
    @FormUrlEncoded
    @POST("otpvalidation")
    Call<DataResponse> logintokenApiCall
            (@Field("UserName") String user,
             @Field("Password") String pass,
             @Field("Grant_type") String gtype);
    @GET("ForgetPassword/getPassword")
    Call<DataResponse> forgetpasswordApiCall (   @Query(value="Id") String emailid);
    @FormUrlEncoded
    @POST("SignUpAgentUser")
    Call<DataResponse> signupApiCall(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("MobileLogout")
    Call<DataResponse> logoutApiCall(@Field("id") String id);



}