package com.knowledgeflex.taskhops.trueface

/*import ai.trueface.sdk.models.*
import ai.trueface.sdk.models.CollectionRequest
import ai.trueface.sdk.models.Collection*/
import com.knowledgeflex.taskhops.models.Enroll
import com.knowledgeflex.taskhops.models.EnrollRequest
import com.knowledgeflex.taskhops.models.Match
import com.knowledgeflex.taskhops.models.MatchRequest
import java.util.concurrent.TimeUnit

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

object Trueface {

    lateinit var endPoints: Services.EndPoints

    var base: String = "https://api.trueface.ai"
    var key: String = "d7NYWRc4x4Zxdb9MIVHxOhdelTDWRTakmjRWHDh0"

    @Suppress("unused")
    class Services {

        interface EndPoints {

            @POST("/v1/match")
            fun match(@Body request: MatchRequest): Call<Match>


        }

        init {
           // val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create()
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder()
                    .addInterceptor { chain ->
                        val builder = chain.request().newBuilder()
                                .addHeader("x-api-key", "d7NYWRc4x4Zxdb9MIVHxOhdelTDWRTakmjRWHDh0")
                                .addHeader("Content-Type", "application/json")
                                .addHeader("Accept-Encoding", "identity")

                        val request = builder.build()
                        chain.proceed(request)
                    }
                    .addInterceptor(interceptor)
                    .retryOnConnectionFailure(true)
                    .connectTimeout(5, TimeUnit.MINUTES)
                    .readTimeout(5, TimeUnit.MINUTES)
                    .readTimeout(5, TimeUnit.MINUTES)
                    .build()

            val retrofit = Retrofit.Builder()
                    .baseUrl(base)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            endPoints = retrofit.create(EndPoints::class.java)
        }

    }
}