package com.knowledgeflex.taskhops.gps;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;

import com.knowledgeflex.taskhops.ApplicationController;
import com.knowledgeflex.taskhops.utils.Logger;
import com.knowledgeflex.taskhops.utils.ObjectUtil;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationParams;


public class GpsTrackingService extends Service {
    private static final String TAG=GpsTrackingService.class.getSimpleName();
    private static final String LOCATION_BROADCAST_ACTION="com.knowledgeflex.taskhops.gps.restart.service";
    private PowerManager.WakeLock mWakeLock;
    private static Location location;


    public GpsTrackingService(Context applicationContext) {
        super();

    }

    public GpsTrackingService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        PowerManager mgr = (PowerManager) getSystemService(Context.POWER_SERVICE);

         /*
        WakeLock is reference counted so we don't want to create multiple WakeLocks. So do a check before initializing and acquiring.
        This will fix the "java.lang.Exception: WakeLock finalized while still held: MyWakeLock" error that you may find.
        */
        if (this.mWakeLock == null) { //**Added this
            this.mWakeLock = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakeLock");
        }

        if (!this.mWakeLock.isHeld()) { //**Added this
            this.mWakeLock.acquire();
        }
        startLocationUpdates();
        return START_STICKY;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Logger.d("EXIT", "ondestroy!");


        if (this.mWakeLock != null) {
            this.mWakeLock.release();
            this.mWakeLock = null;
        }
        stopLocationUpdates();
        Intent broadcastIntent = new Intent(LOCATION_BROADCAST_ACTION);
        sendBroadcast(broadcastIntent);
    }






    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    public void startLocationUpdates() {


        try {
            stopLocationUpdates();
            LocationParams params = LocationParams.NAVIGATION;
            SmartLocation.with(ApplicationController.getInstance()).location().config(params).start(new OnLocationUpdatedListener() {
                @Override
                public void onLocationUpdated(Location location) {
                    Logger.d(TAG, location.toString());
                    GpsTrackingService.this.location = location;

                }
            });

            //SmartLocation.with(ApplicationController.getInstance()).location(LocationManager.NETWORK_PROVIDER).getLastLocation();
        }
        catch (Exception e){
            e.printStackTrace();
        }


    }

    protected void stopLocationUpdates() {
        SmartLocation.with(ApplicationController.getInstance()).location().stop();
    }

    public static Location getLocation() {
        if(ObjectUtil.isNull(location)){
            location=  getLocationTrack();
        }
        return location;
    }


    private static Location getLocationTrack(){

        LocationTrack locationTrack = new LocationTrack(ApplicationController.getInstance());
        if(locationTrack.canGetLocation){
            return locationTrack.getLoc();
        }
        else {
            return  null;
        }

    }

    public static void setLocation(Location loc){
        location=loc;
    }

   

}
