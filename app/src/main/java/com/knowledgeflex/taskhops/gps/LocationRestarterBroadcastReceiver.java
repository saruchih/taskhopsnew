package com.knowledgeflex.taskhops.gps;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.knowledgeflex.taskhops.utils.Logger;


public class LocationRestarterBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Logger.d(LocationRestarterBroadcastReceiver.class.getSimpleName(), "Service Stops! Oooooooooooooppppssssss!!!!");
        context.startService(new Intent(context, GpsTrackingService.class));;
    }
}